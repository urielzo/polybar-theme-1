# polybar theme-1

one theme for polybar

## Preview

## clean
![light](/preview/Screenshot_1600x900.png)
<br />

## Fonts

- **JetBrainsMono**
- **Font Awesome 5 Free**
- **JetBrainsMono Nerd Font**
- **FontAwesome**
- **Noto Sans Mono CJK JP**

## Details
- **Distro** ArchLabs ;)
- **WM** berry
- **Panel** Polybar
- **Program Launcher** rofi

## add this line to your ~/.config/berry/autostart

## ~/.config/polybar/polybar_wrapper launch &
