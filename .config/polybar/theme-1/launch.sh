#!/bin/bash
# Terminate already running bar instances
    killall -q polybar

# Wait until the processes have been shut down
    while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar, using default config location ~/.config/polybar/config
polybar -c ~/.config/polybar/theme-1/config bar-right-background &
polybar -c ~/.config/polybar/theme-1/config bar-right &

polybar -c ~/.config/polybar/theme-1/config bar-left-background &
polybar -c ~/.config/polybar/theme-1/config bar-left &

polybar -c ~/.config/polybar/theme-1/config bar-center-background &
